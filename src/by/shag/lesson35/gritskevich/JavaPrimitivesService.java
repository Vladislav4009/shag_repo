package by.shag.lesson35.gritskevich;

import java.sql.*;
import java.util.HashSet;
import java.util.Set;

public class JavaPrimitivesService {

    private ConnectionCreator creator = new ConnectionCreator();

    public JavaPrimitivesService() {
        createPrimitivesTable();
    }

    public void createPrimitivesTable() {

        String sql = "create table if not exists primitives(\n" +
                "id serial primary key,\n" +
                "name varchar(20),\n" +
                "size_in_byte integer,\n" +
                "unique (name, size_in_byte)\n" +
                ");";
        String sqlFill = "insert into primitives (name, size_in_byte) values ('byte', 8) on conflict do nothing;\n" +
                "insert into primitives (name, size_in_byte) values ('short', 16) on conflict do nothing;\n" +
                "insert into primitives (name, size_in_byte) values ('char', 16) on conflict do nothing;\n" +
                "insert into primitives (name, size_in_byte) values ('int', 32) on conflict do nothing;\n" +
                "insert into primitives (name, size_in_byte) values ('long', 64) on conflict do nothing;\n" +
                "insert into primitives (name, size_in_byte) values ('float', 32) on conflict do nothing;\n" +
                "insert into primitives (name, size_in_byte) values ('double', 64) on conflict do nothing;\n" +
                "insert into primitives (name, size_in_byte) values ('boolean', 32) on conflict do nothing;";

        try (Connection connection = creator.createConnection();
             Statement statement = connection.createStatement()) {

            statement.addBatch(sql);
            statement.addBatch(sqlFill);
            statement.executeBatch();

        } catch (SQLException e) {

        }
    }

    public Set<Primitive> getAllJavaPrimitives() {

        Set<Primitive> primitives = new HashSet<>();

        try (Connection connection = creator.createConnection();
             Statement statement = connection.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_UPDATABLE);
             ResultSet resultSet = statement.executeQuery("select * from primitives ")) {

            while (resultSet.next()) {

                primitives.add(handleResultSetRecord(resultSet));

            }
        } catch (SQLException e) {

        }
        return primitives;
    }

    private Primitive handleResultSetRecord(ResultSet resultSet) throws SQLException {
        int id = resultSet.getInt("id");
        String name = resultSet.getString("name");
        int size = resultSet.getInt("size_in_byte");

        return new Primitive(id, name, size);
    }

    public Integer getSizeInBytesByName(String name) {
        int size = 0;
        String sql = "select size_in_byte from primitives where name like ? ";
        try (Connection connection = creator.createConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {

            preparedStatement.setString(1, name);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {

                size = resultSet.getInt("size_in_byte");

            }
        } catch (Exception e) {

        }
        return size;
    }
}
