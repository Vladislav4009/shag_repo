package by.shag.lesson35.gritskevich;

public class Runner {
    public static void main(String[] args) {

        JavaPrimitivesService javaPrimitivesService = new JavaPrimitivesService();
        System.out.println(javaPrimitivesService.getAllJavaPrimitives());
        System.out.println(javaPrimitivesService.getSizeInBytesByName("int"));

    }
}