package by.shag.lesson35.gritskevich;

public class Primitive {

    private Integer id;
    private String name;
    private Integer size;

    public Primitive(Integer id, String name, Integer size) {
        this.id = id;
        this.name = name;
        this.size = size;
    }

    @Override
    public String toString() {
        return "Primitive{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", size=" + size +
                '}';
    }
}
