package by.shag.lesson24.gritskevich;

import java.io.*;
import java.nio.charset.StandardCharsets;

public class FileManager {

    public static void fileWithNameCreate(String fileName) {
        try (OutputStream out = new FileOutputStream(createFullFileName(fileName))) {
            String authorFullName = "Gritskevich Vlad";
            out.write(authorFullName.getBytes());
        } catch (InterruptedIOException e) {
            System.out.println("I/O operation has been interrupted");
            e.printStackTrace();
        } catch (EOFException e) {
            System.out.println("end of file or end of stream has been reached unexpectedly during input");
            e.printStackTrace();
        } catch (CharConversionException e) {
            System.out.println("character conversion exceptions");
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            System.out.println("file not found");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("exception during the close input stream");
            e.printStackTrace();
        }
    }

    public static void fileWithIntegerCreate(String fileName) {
        final int LINE_TRANSLATION_ASCII_TABLE_KEY = 10;

        try (OutputStream out = new FileOutputStream(createFullFileName(fileName))) {
            for (int i = 0; i < 101; i++) {
                if (i % 2 == 0) {
                    String a = String.valueOf(i);
                    out.write(a.getBytes());
                    out.write(LINE_TRANSLATION_ASCII_TABLE_KEY);
                }
            }
        } catch (InterruptedIOException e) {
            System.out.println("I/O operation has been interrupted");
            e.printStackTrace();
        } catch (EOFException e) {
            System.out.println("end of file or end of stream has been reached unexpectedly during input");
            e.printStackTrace();
        } catch (CharConversionException e) {
            System.out.println("character conversion exceptions");
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            System.out.println("file not found");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("exception during the close input stream");
            e.printStackTrace();
        }
    }

    public static void fileWithNameThatFileCreate(String fileName) {
        try (OutputStream out = new FileOutputStream(createFullFileName(fileName))) {
//            out.write(Character.getNumericValue(fileName.toCharArray());
        } catch (InterruptedIOException e) {
            System.out.println("I/O operation has been interrupted");
            e.printStackTrace();
        } catch (EOFException e) {
            System.out.println("end of file or end of stream has been reached unexpectedly during input");
            e.printStackTrace();
        } catch (CharConversionException e) {
            System.out.println("character conversion exceptions");
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            System.out.println("file not found");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("exception during the close input stream");
            e.printStackTrace();
        }
    }

    public static String createFullFileName(String fileName) {
        String fileDirectory = System.getProperty("user.dir") + "/src/by/shag/lesson24";
        return fileDirectory + File.separator + fileName;
    }
}
