package by.shag.kursach.gritskevich;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class CallCenter {

    ExecutorService executorService;

    public CallCenter() {
        executorService = Executors.newFixedThreadPool(5);
        System.out.println("CallCenter create");
    }

    public void launchCallCenter(ClientBlockingQueue clientBlockingQueue) {

        for (int i = 1; i <= 5; i++) {
            executorService.execute(new Thread(new Operator("Operator " + i, clientBlockingQueue)));
        }

    }

    public void stopCallCenter() {
        executorService.shutdown();
    }
}