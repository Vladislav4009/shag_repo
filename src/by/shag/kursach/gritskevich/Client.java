package by.shag.kursach.gritskevich;

public class Client implements Runnable {

    @Override
    public void run() {
        try {
            System.out.println("Client " + Thread.currentThread().getName() + " start");
            Thread.sleep((long) ((Math.random() * 5) + 3) * 1_000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
