package by.shag.kursach.gritskevich;

public class Operator implements Runnable {

    private final ClientBlockingQueue clientBlockingQueue;
    private String name;

    public Operator(String name, ClientBlockingQueue clientBlockingQueue) {
        this.clientBlockingQueue = clientBlockingQueue;
        this.name = name;
        new Thread(this).start();
        System.out.println(name + " start");
    }

    @Override
    public void run() {
        clientBlockingQueue.getFromThreadQueue(name);
    }
}
