package by.shag.kursach.gritskevich;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class ClientBlockingQueue {

    private final BlockingQueue<Thread> threadBlockingQueue;

    public ClientBlockingQueue() {
        threadBlockingQueue = new ArrayBlockingQueue<>(15);
    }

    public BlockingQueue<Thread> getThreadBlockingQueue() {
        return threadBlockingQueue;
    }

    public void addToThreadQueue(Thread thread) {
//        System.out.println(threadBlockingQueue.size() + " start");
//        System.out.println(threadBlockingQueue.toString());
        if (!threadBlockingQueue.offer(thread)) {
//            System.out.println(threadBlockingQueue.size() + " before if");
//            System.out.println(threadBlockingQueue.toString());
            System.out.println("Очередь клиентов заполнена.");
            try {
                System.out.println(threadBlockingQueue.take().getName() + " excluded.");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
//            System.out.println(threadBlockingQueue.size() + " after if");
//            System.out.println(threadBlockingQueue.toString());
            threadBlockingQueue.offer(thread);
            System.out.println(thread.getName() + " included.");
//            System.out.println(threadBlockingQueue.size() + " after offer again");
//            System.out.println(threadBlockingQueue.toString());
        }
    }

    public void getFromThreadQueue(String operatorName) {
        try {
            Thread thread = threadBlockingQueue.take();
            System.out.println(operatorName + " ACCEPTED call client " + thread.getName());
            thread.start();
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
