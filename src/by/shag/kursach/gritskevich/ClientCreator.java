package by.shag.kursach.gritskevich;

public class ClientCreator implements Runnable {

    private final ClientBlockingQueue clientBlockingQueue;
    private final Integer CLIENT_QUANTITY =  ((int) (Math.random() * 51) + 50);

    public ClientCreator(ClientBlockingQueue clientBlockingQueue) {
        this.clientBlockingQueue = clientBlockingQueue;
        System.out.println("Старт потока генерации клиентов");

    }

    @Override
    public void run() {
        createClient(clientBlockingQueue);
    }

    public void createClient(ClientBlockingQueue clientBlockingQueue) {
        for (int i = 0; i < CLIENT_QUANTITY; i++) {

            Thread thread = new Thread(new Client());
            System.out.println("Create client " + thread.getName());
            System.out.println("Client " + thread.getName() + " call to call center");
            clientBlockingQueue.addToThreadQueue(thread);
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }
}
