package by.shag.lesson25.gritskevich;

import java.util.List;

public class SumOfNumberInMasThread implements Runnable {

    private List<Integer> integerArrayListForThread;
    private RepositoryOfSumElements repository;

    public SumOfNumberInMasThread(List<Integer> integerArrayListForThread, RepositoryOfSumElements repository) {
        this.integerArrayListForThread = integerArrayListForThread;
        this.repository = repository;
    }

    @Override
    public void run() {

        int sum = 0;

        for (int i = 0; i < integerArrayListForThread.size(); i++) {
            sum += integerArrayListForThread.get(i);
        }

        System.out.println(sum);
        repository.setFullSum(sum);
    }
}
