package by.shag.lesson25.gritskevich;

public class PrintNameAndStringOfThreat extends Thread {

    private String desc;
    private int iterationQuantity;

    public PrintNameAndStringOfThreat(String desc, int iterationQuantity) {
        this.desc = desc;
        this.iterationQuantity = iterationQuantity;
    }

    @Override
    public void run() {
        for (int i = 0; i < iterationQuantity; i++) {
            System.out.println(this.getName() + desc);
        }
    }
}
