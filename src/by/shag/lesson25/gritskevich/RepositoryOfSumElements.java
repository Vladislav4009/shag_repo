package by.shag.lesson25.gritskevich;

import java.util.concurrent.atomic.AtomicInteger;

public class RepositoryOfSumElements {

    private AtomicInteger fullSum = new AtomicInteger(0);

    public AtomicInteger getFullSum() {
        return fullSum;
    }

    public void setFullSum(int number) {
        this.fullSum.addAndGet(number);
    }
}
