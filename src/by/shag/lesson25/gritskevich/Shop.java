package by.shag.lesson25.gritskevich;

import java.util.concurrent.atomic.AtomicInteger;

public class Shop {

    private final int MAX_CLOTHES_QUANTITY = 50;
    private AtomicInteger clothesQuantity;

    public Shop(AtomicInteger clothesQuantity) {
        this.clothesQuantity = clothesQuantity;
    }

    public synchronized void getClothesQuantity() throws InterruptedException {
        while (clothesQuantity.get() < 1) {
            System.out.println("Товара нет");
            wait();
        }
        clothesQuantity.decrementAndGet();
        notifyAll();
    }

    public synchronized void setClothesQuantity() throws InterruptedException {
        while (clothesQuantity.get() >= MAX_CLOTHES_QUANTITY) {
            System.out.println("Склад полон");
            wait();
        }
        clothesQuantity.addAndGet(10);
        notifyAll();
    }
}