package by.shag.lesson25.gritskevich;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class CreateFilesThread extends Thread {

    private List<String> stringArrayListForThread;

    public CreateFilesThread(List<String> stringArrayListForThread) {
        this.stringArrayListForThread = stringArrayListForThread;
    }

    @Override
    public void run() {

        StringBuilder stringBuilder = new StringBuilder();

        for (String s : stringArrayListForThread) {
            stringBuilder.append(s);
        }

        String fileName = stringBuilder.toString();
        File file = new File(System.getProperty("user.dir") + "/src/by/shag/lesson25"
                + File.separator + (fileName + ".txt"));

        try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}