package by.shag.lesson25.gritskevich;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Exchanger;
import java.util.concurrent.atomic.AtomicInteger;

public class Runner {
    public static void main(String[] args) {

        withFourParametersMethod(5, " имя потока", 2, true); // Первое задание


        List<String> stringArrayList = new ArrayList<>(); // Второе задание
        String str = "ублюдокматьтвоюануидисюдаговнособачьечерешилменятрахнутьятебясамтрахну";
        for (int i = 0; i < str.length(); i++) {
            stringArrayList.add(String.valueOf(str.charAt(i)));
        }

        int threadQuantity = 4;

        filesCreate(stringArrayList, threadQuantity);



        int arraySize = 3_400; // Задание три и четыре
        List<Integer> integerArrayList = new ArrayList<>(arraySize);
        for (int i = 0; i < arraySize; i++) {
            integerArrayList.add((int) ((Math.random() * 1000) - 500));
        }

        getSumOfMass(integerArrayList);



        copyOfFile(); // Задание четыре
        // С копированием не справился, копируется только первая строчка и все, не додумался до верного рещения. Извините)



        shopRun(); // Задание пять


    }

    private static void withFourParametersMethod(int threadsQuantity, String desc, int iterationQuantity, boolean createEndString) {

        if (threadsQuantity > 0) {

            List<PrintNameAndStringOfThreat> threadList = new ArrayList<>();

            for (int i = 0; i < threadsQuantity; i++) {
                PrintNameAndStringOfThreat printNameAndStringOfThreat = new PrintNameAndStringOfThreat(desc, iterationQuantity);
                threadList.add(printNameAndStringOfThreat);
                printNameAndStringOfThreat.start();
            }

            if (createEndString) {

                for (int i = 0; i < threadsQuantity; i++) {

                    try {
                        threadList.get(i).join();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }

            System.out.println("Конец");
        }
    }

    private static void filesCreate(List<String> stringArrayList, int threadQuantity) {

        int forOneThreadMasSize = stringArrayList.size() / threadQuantity;
        int a = stringArrayList.size() % threadQuantity;

        for (int i = 0; i < threadQuantity; i++) {
            List<String> stringArrayListForThread = new ArrayList<>();

            for (int j = 0; j < forOneThreadMasSize; j++) {
                stringArrayListForThread.add(stringArrayList.get(0));
                stringArrayList.remove(0);
            }

            CreateFilesThread thread = new CreateFilesThread(stringArrayListForThread);
            thread.start();
        }
    }

    private static void getSumOfMass(List<Integer> integerArrayList) {

        final int THREAT_LIMITER = 1_000;
        int threadQuantity;

        RepositoryOfSumElements repository = new RepositoryOfSumElements();

        threadQuantity = (integerArrayList.size() / THREAT_LIMITER) + 1;

        List<Thread> threadList = new ArrayList<>(threadQuantity);

        for (int i = 0; i < threadQuantity; i++) {
            List<Integer> integerArrayListForThread = new ArrayList<>();

            for (int j = 0; j < THREAT_LIMITER; j++) {
                if (integerArrayList.size() > 0) {
                    integerArrayListForThread.add(integerArrayList.get(0));
                    integerArrayList.remove(0);
                }
            }

            Thread thread = new Thread(new SumOfNumberInMasThread(integerArrayListForThread, repository));
            threadList.add(thread);
            thread.start();
        }

        for (Thread thread : threadList) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        System.out.println(repository.getFullSum());
    }

    private static void copyOfFile() {

        for (int i = 0; i < 5; i++) {
            Exchanger<String> exchanger = new Exchanger<>();
            new Thread(new ReadFileThread(exchanger)).start();
            new Thread(new WriteFileThread(exchanger)).start();
        }
    }

    private static void shopRun() {

        Shop shop = new Shop(new AtomicInteger(25));

        for (int i = 0; i < 1000; i++) {
            CustomerThread customerThread = new CustomerThread(shop);
            new Thread(customerThread).start();
        }

        for (int i = 0; i < 100; i++) {
            ProviderThread providerThread = new ProviderThread(shop);
            new Thread(providerThread).start();
        }
    }
}
