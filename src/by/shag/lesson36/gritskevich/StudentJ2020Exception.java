package by.shag.lesson36.gritskevich;

public class StudentJ2020Exception extends Exception {

    public StudentJ2020Exception(Throwable cause) {
        super(cause);
    }
}
