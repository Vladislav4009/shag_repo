package by.shag.lesson36.gritskevich;

public class Runner {
    public static void main(String[] args) {

        StudentJ2020Service studentJ2020Service = new StudentJ2020Service();
        try {
            studentJ2020Service.create(new StudentJ2020(null, "Владислав", "Грицкевич", "+375(29)859-40-09"));
            studentJ2020Service.create(new StudentJ2020(null, "Дмитрий", "Рафалович", "+375(29)246-84-98"));
            studentJ2020Service.create(new StudentJ2020(null, "Евгений", "Карпович", "+375(33)629-27-14"));
            studentJ2020Service.create(new StudentJ2020(null, "Антон", "Литвинчук", "+375(25)627-63-48"));
            studentJ2020Service.create(new StudentJ2020(null, "Мария", "Голятина", "+375(29)110-06-41"));
            studentJ2020Service.create(new StudentJ2020(null, "Анжела", "Шустова", "+375(29)349-40-25"));
            studentJ2020Service.create(new StudentJ2020(null, "Сергей", "Тимчук", "+375(29)610-53-30"));
            studentJ2020Service.create(new StudentJ2020(null, "Дима", "Данилович", "+375(29)778-54-81"));
            studentJ2020Service.create(new StudentJ2020(null, "Максим", "Клецко", "+375(29)167-16-78"));
            studentJ2020Service.create(new StudentJ2020(null, "Егор", "Савостьянчик", "+375(29)350-24-58"));

//            final int SEARCH_TEST_ID = 1;
//            StudentJ2020 studentJ2020 = studentJ2020Service.findById(SEARCH_TEST_ID);
//            studentJ2020.setId(SEARCH_TEST_ID);
//            System.out.println(studentJ2020);

//            final String SEARCH_TEST_STRING = "+375(29)859-40-09";
//            StudentJ2020 studentJ2020 = studentJ2020Service.findByPhoneNumber(SEARCH_TEST_STRING);
//            System.out.println(studentJ2020);

//            final String SEARCH_TEST_NAME = "Дмитрий";
//            List<StudentJ2020> studentJ2020list = studentJ2020Service.findByName(SEARCH_TEST_NAME);
//            System.out.println(studentJ2020list);

//            studentJ2020Service.updateStudent(new StudentJ2020(1, "Владислав", "Грицкевич", "+375(29)859-40-09"));

//            studentJ2020Service.deleteById(1);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
