package by.shag.lesson36.gritskevich;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class StudentJ2020Service {

    private final String PREPARED_INSERT =
            "insert into j2020(name, last_name, phone_number) values (?, ?, ?) on conflict do nothing";

    private ConnectionCreator creator = new ConnectionCreator();

    public StudentJ2020 create(StudentJ2020 student) throws Exception {

        Connection ourConnection = null;
        try (Connection connection = creator.createConnection();
             PreparedStatement statement = connection.prepareStatement(PREPARED_INSERT, Statement.RETURN_GENERATED_KEYS)) {

            ourConnection = connection;
            connection.setAutoCommit(false);
            statement.setString(1, student.getName());
            statement.setString(2, student.getLastName());
            statement.setString(3, student.getPhoneNumber());
            statement.executeUpdate();
            connection.commit();
            ResultSet generatedKeys = statement.getGeneratedKeys();

            if (generatedKeys.next()) {
                int key = generatedKeys.getInt(1);
                student.setId(key);
            }

        } catch (SQLException e) {
            if (ourConnection != null) {
                ourConnection.rollback();
            }
            throw new StudentJ2020Exception(e);
        }
        return student;
    }

    public StudentJ2020 findById(int id) throws StudentJ2020Exception {

        StudentJ2020 studentJ2020 = null;

        try (Connection connection = creator.createConnection();
             PreparedStatement statement = connection.prepareStatement(
                     "select name, last_name, phone_number from j2020 where id = ?")) {

            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                String name = resultSet.getString("name");
                String lastName = resultSet.getString("last_name");
                String phoneNumber = resultSet.getString("phone_number");
                studentJ2020 = new StudentJ2020(id, name, lastName, phoneNumber);
            }

        } catch (SQLException e) {
            throw new StudentJ2020Exception(e);
        }
        return studentJ2020;
    }

    public StudentJ2020 findByPhoneNumber(String phoneNumber) throws StudentJ2020Exception {

        StudentJ2020 studentJ2020 = null;

        try (Connection connection = creator.createConnection();
             PreparedStatement statement = connection.prepareStatement(
                     "select id, name, last_name from j2020 where phone_number like ? ")) {

            statement.setString(1, phoneNumber);
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                String lastName = resultSet.getString("last_name");
                studentJ2020 = new StudentJ2020(id, name, lastName, phoneNumber);
            }

        } catch (SQLException e) {
            throw new StudentJ2020Exception(e);
        }
        return studentJ2020;
    }

    public List<StudentJ2020> findByName(String name) throws StudentJ2020Exception {

        List<StudentJ2020> studentJ2020List = new ArrayList<>();

        try (Connection connection = creator.createConnection();
             PreparedStatement statement = connection.prepareStatement(
                     "select id, last_name, phone_number from j2020 where name like ?")) {

            statement.setString(1, name);
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                Integer id = resultSet.getInt("id");
                String lastName = resultSet.getString("last_name");
                String phoneNumber = resultSet.getString("phone_number");
                studentJ2020List.add(new StudentJ2020(id, name, lastName, phoneNumber));
            }

        } catch (SQLException e) {
            throw new StudentJ2020Exception(e);
        }
        return studentJ2020List;
    }

    public void updateStudent(StudentJ2020 student) throws StudentJ2020Exception, SQLException {

        Connection ourConnection = null;
        try (Connection connection = creator.createConnection();
             PreparedStatement statement = connection.prepareStatement(
                     "update j2020 set name = ?, last_name = ?, phone_number = ? where id = ?")) {

            ourConnection = connection;
            connection.setAutoCommit(false);
            statement.setString(1, student.getName());
            statement.setString(2, student.getLastName());
            statement.setString(3, student.getPhoneNumber());
            statement.setInt(4, student.getId());
            statement.executeUpdate();
            connection.commit();

        } catch (SQLException e) {
            if (ourConnection != null) {
                ourConnection.rollback();
            }
            throw new StudentJ2020Exception(e);
        }
    }

    public void deleteById(int id) throws StudentJ2020Exception {

        try (Connection connection = creator.createConnection();
             PreparedStatement statement = connection.prepareStatement(
                     "delete from j2020 where id = ?")) {

            statement.setInt(1, id);
            statement.executeUpdate();

        } catch (SQLException e) {
            throw new StudentJ2020Exception(e);
        }
    }
}
