package lesson30.gritskevich;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class RegistrationService {

    private final List<String> takenEmails = new ArrayList<>();

    private static final String SUCCESS_MESSAGE = "Вы успешно зарегестрированы!";


    // Test this method :)

    public String register(String name, String email, String password, LocalDate birthDate) {

        // Здесь могла быть валидация на Null и на пустые строки (можете их добавить сами когда все остальное сделаете)



        if (birthDate.getYear() > 2000) {

            // throw your own Exception!

        }



        if (!email.contains("@")) {

            // throw your own Exception!

        }



        if (password.length() < 6) {

            // throw your own Exception!

        }



        if (takenEmails.contains(email)) {

            // throw your own Exception!

        }



        takenEmails.add(email); // SUCCESS!



        return SUCCESS_MESSAGE;

    }

}
